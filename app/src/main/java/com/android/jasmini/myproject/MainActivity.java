package com.android.jasmini.myproject;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Arrays;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class MainActivity extends AppCompatActivity {

    public static final int MEDIA_TYPE_PICTURE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    private static final int CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE = 200;
    private static final int RequestPermissionCode = 1;
    private static final String TAG = "MYAPP";
    // private static final String FILEPATH = "filepath";

    FFmpeg ffmpeg;
    File videoFile;
    private String compressedVideoPath;
    private String watermarkedVideoPath;

    // private int choice = 0;
    private ProgressDialog progressDialog;

    public static File getFile(String filename, int type) {
        String folder;
        switch (type) {
            case MEDIA_TYPE_VIDEO:
                folder = Environment.DIRECTORY_MOVIES;
                break;
            case MEDIA_TYPE_PICTURE:
                folder = Environment.DIRECTORY_PICTURES;
                break;
            default:
                folder = Environment.DIRECTORY_DOWNLOADS;
        }
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                folder), "MyApp");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.e(TAG, "Failed to create directory MyApp.");
                return null;
            }
        }
        return new File(mediaStorageDir.getPath() + File.separator + filename);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(null);
        progressDialog.setCancelable(false);
        loadFFMpegBinary();
        if (!checkPermission()) {
            requestPermission();

        } else {
            saveImageForWaterMark();
            recordVideo();
        }
    }

    private void saveImageForWaterMark() {
        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.newlogo);


        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        Log.e("extStorageDirectory", extStorageDirectory + "");


        File file = new File(extStorageDirectory, "newlogo.png");
        FileOutputStream outStream;
        try {
            outStream = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void recordVideo() {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

        videoFile = getOutputMediaFile(MEDIA_TYPE_VIDEO);
        assert videoFile != null;
        Uri fileUri = FileProvider.getUriForFile(MainActivity.this,
                "com.android.jasmini.myproject.fileprovider", videoFile);
        // set the video file name
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        // set the video image quality to high
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
        // start the Video Capture Intent
        startActivityForResult(intent, CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE);
    }


    /**
     * Create a File for saving an image or video
     */
    private File getOutputMediaFile(int type) {

        File mediaStorageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        // Create the storage directory(MyCameraVideo) if it does not exist
        assert mediaStorageDir != null;
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Toast.makeText(MainActivity.this, "Failed to create directory MyCameraVideo.",
                        Toast.LENGTH_LONG).show();
                Log.e("MyCameraVideo", "Failed to create directory MyCameraVideo.");
                return null;
            }
        }
        // Create a media file name
        // For unique file name appending current timeStamp with file name
        java.util.Date date = new java.util.Date();
        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(date.getTime());
        File mediaFile;
        if (type == MEDIA_TYPE_VIDEO) {
            // For unique video file name appending current timeStamp with file name
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_" + timeStamp + ".mp4");

        } else {
            return null;
        }
        return mediaFile;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // After camera screen this code will executed
        if (requestCode == CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE) {
            Log.e("***","****");
            if (resultCode == RESULT_OK) {

                Toast.makeText(this, "Video saved to:" +
                        data.getData(), Toast.LENGTH_LONG).show();
                Uri selectedVideoUri = data.getData();

                Log.e("selectedUri", selectedVideoUri + "");
                java.util.Date date = new java.util.Date();
                @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                        .format(date.getTime());
                File compressedVideo = getFile("VID_compressed_" + timeStamp + ".mp4", MEDIA_TYPE_VIDEO);
                File watermarkedVideo = getFile("VID_watermarked_" + timeStamp + ".mp4", MEDIA_TYPE_VIDEO);
                Log.e("compressedVideo", compressedVideo + "");
                assert compressedVideo != null;
                compressedVideoPath = compressedVideo.getAbsolutePath();

                Log.e("destination path", compressedVideoPath + "");
                assert watermarkedVideo != null;
                watermarkedVideoPath = watermarkedVideo.getAbsolutePath();

                assert selectedVideoUri != null;
                executeCompressCommand(selectedVideoUri.getPath(), compressedVideo.getAbsolutePath());


            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "User cancelled the video capture.",
                        Toast.LENGTH_LONG).show();

            } else {
                Toast.makeText(this, "Video capture failed.",
                        Toast.LENGTH_LONG).show();
            }
        }


    }


    private void requestPermission() {
        ActivityCompat.requestPermissions(MainActivity.this, new String[]
                {
                        WRITE_EXTERNAL_STORAGE, RECORD_AUDIO, CAMERA

                }, RequestPermissionCode);

    }
    //set  permission

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case RequestPermissionCode:
                if (grantResults.length > 0) {
                    boolean permission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean permission1 = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean permission2 = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    if (permission && permission1 && permission2) {
                        Toast.makeText(MainActivity.this, "Permission Granted", Toast.LENGTH_LONG).show();
                        saveImageForWaterMark();
                        recordVideo();
                    } else {
                        Toast.makeText(MainActivity.this, "Permission Denied", Toast.LENGTH_LONG).show();

                    }
                }
                break;
        }
    }

    public boolean checkPermission() {
        int firstPermissionResult = ContextCompat.checkSelfPermission(MainActivity.this, WRITE_EXTERNAL_STORAGE);
        int secondPermissionResult = ContextCompat.checkSelfPermission(MainActivity.this, RECORD_AUDIO);
        int thirdPermissionResult = ContextCompat.checkSelfPermission(MainActivity.this, CAMERA);
        return firstPermissionResult == PackageManager.PERMISSION_GRANTED && secondPermissionResult == PackageManager.PERMISSION_GRANTED && thirdPermissionResult == PackageManager.PERMISSION_GRANTED;
    }


    /**
     * Load FFmpeg binary
     */
    private void loadFFMpegBinary() {
        try {
            if (ffmpeg == null) {
                Log.e(TAG, "ffmpeg : Not loaded");
                ffmpeg = FFmpeg.getInstance(this);
            }
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {
                @Override
                public void onFailure() {
                    showUnsupportedExceptionDialog();
                }

                @Override
                public void onSuccess() {
                    Log.e(TAG, "ffmpeg : Loaded");
                }
            });
        } catch (FFmpegNotSupportedException e) {
            showUnsupportedExceptionDialog();
        } catch (Exception e) {
            Log.e(TAG, "ffmpeg Exception: " + e);
        }
    }

    private void showUnsupportedExceptionDialog() {
        new AlertDialog.Builder(MainActivity.this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Not Supported")
                .setMessage("Device Not Supported")
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.this.finish();
                    }
                })
                .create()
                .show();

    }


    private void execFFmpegBinary(final String[] command, final boolean executeWatermark) {
        try {
            ffmpeg.execute(command, new ExecuteBinaryResponseHandler() {
                @Override
                public void onFailure(String s) {
                    Log.e(TAG, "FAILED with output : " + s);
                }

                @Override
                public void onSuccess(String s) {
                    Log.e(TAG, "SUCCESS with output : " + s);


                    if (executeWatermark) {
                        executeWatermarkCommand(compressedVideoPath, watermarkedVideoPath);
                    }
                }

                @Override
                public void onProgress(String s) {
                    Log.e(TAG, "Started command : ffmpeg " + Arrays.toString(command));
                    progressDialog.setMessage("progress : splitting video " + s);

                    Log.e(TAG, "progress : " + s);
                }

                @Override
                public void onStart() {
                    Log.e(TAG, "Started command : ffmpeg " + Arrays.toString(command));
                    progressDialog.setMessage("Processing...");
                    progressDialog.show();

                }

                @Override
                public void onFinish() {
                    Log.e(TAG, "Finished command : ffmpeg " + Arrays.toString(command));
                    progressDialog.dismiss();
                   // finish();


                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            // do nothing for now
        }
    }

    /**
     * Command for compressing video
     */
    private void executeCompressCommand(String sourcePath, String destinationPath) {
        Log.e(TAG, "ffmpeg compress: src: " + sourcePath);
        Log.e(TAG, "ffmpeg compress: dest: " + destinationPath);
        String str = Environment.getExternalStorageDirectory().toString() + "/Android/data/com.android.jasmini.myproject/files" + sourcePath;
        String[] complexCommand = {"-y", "-i", str, "-s", "300x300", "-r", "8", "-vcodec", "mpeg4", "-b:v", "150k", "-b:a", "24000", "-ac", "2", "-ar", "22050", destinationPath};
        execFFmpegBinary(complexCommand, true);
    }

    /**
     * Command for compressing video
     */
    private void executeWatermarkCommand(String sourcePath, String destinationPath) {

        String imagePath = Environment.getExternalStorageDirectory().toString() + "/newlogo.png";
        Log.e(TAG, "watermark: imagePath: " + imagePath);
        Log.e(TAG, "watermark: src: " + sourcePath);
        Log.e(TAG, "watermark: dest: " + destinationPath);
        String[] complexCommand = {"-i", sourcePath, "-i", imagePath, "-filter_complex", "overlay=5:main_h-overlay_h-5", destinationPath};
        execFFmpegBinary(complexCommand, false);
    }
}

